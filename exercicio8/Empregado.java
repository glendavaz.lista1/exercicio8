public class Empregado
{
    private int idade;
    
  
    public Empregado()
    {
  
    }  
    
    public int getIdade() {
        return this.idade;
    }
    
    public void setIdade(int paramIdade) {
        this.idade = paramIdade;
    }
    
    public boolean calculoAposentadoria(int paramTempoTrabalhado) {
        if(this.idade >= 65 || paramTempoTrabalhado >= 30 || (this.idade >= 60 && paramTempoTrabalhado >= 25)) {
            return true;
        }
        return false;
    }
}
